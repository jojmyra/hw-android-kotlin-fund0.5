package com.example.android.guesstheword.screens.score

import android.os.Parcel
import android.os.Parcelable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ScoreViewModelFactory(private val finalScore: Int): ViewModelProvider.Factory, Parcelable {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ScoreViewModel::class.java)) {
            return ScoreViewModel(finalScore) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

    constructor(parcel: Parcel) : this(parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(finalScore)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ScoreViewModelFactory> {
        override fun createFromParcel(parcel: Parcel): ScoreViewModelFactory {
            return ScoreViewModelFactory(parcel)
        }

        override fun newArray(size: Int): Array<ScoreViewModelFactory?> {
            return arrayOfNulls(size)
        }
    }
}